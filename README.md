## CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Maintainers


## INTRODUCTION
------------

* The Commerce Shipping Linear Weight (commerce_shipping_linear_weight) is a module that offers a shipping method plugin
  to compute the rate based on the weight of the package using a linear relationship.
  For instance, assuming a value of 2 €/kg a order of 2 kg will result in a rate of 4 €.
  The module assumes all weights are in kg.


## REQUIREMENTS
------------

* This module requires the Commerce Shipping module.


## INSTALLATION
------------

 * Install as you would normally install a contributed Drupal module. Visit
   https://www.drupal.org/docs/8/extending-drupal-8/installing-drupal-8-modules
   for further information.


## MAINTAINERS
-----------

 * Current maintainers: [nsalves](https://www.drupal.org/u/nsalves)

This project has been sponsored by:
 * NTT DATA
    NTT DATA – a part of NTT Group – is a trusted global innovator of IT and business services headquartered in Tokyo.
    NTT is one of the largest IT services provider in the world and has 140,000 professionals, operating in more than 50 countries.
    NTT DATA supports clients in their digital development through a wide range of consulting and strategic advisory services, cutting-edge technologies, applications, infrastructure, modernization of IT and BPOs.
    We contribute with vast experience in all sectors of economic activity and have extensive knowledge of the locations in which we operate
